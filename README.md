# unixcourse

Courseware for "Introduction to the UNIX command line"

## Required Software

    * Windows: Windows Subsystem for Linux (WSL) with Ubuntu or Debian. To install, follow this guide: https://www.howtogeek.com/249966/how-to-install-and-use-the-linux-bash-shell-on-windows-10/
    * Mac/Linux: No special requirements, just a terminal window.

## Syllabus:
    * Why on earth should I use the command line? 
    * What's in my computer? Disks, directories, files
    * Lifting the confusion: Terminal, console, command line, shell, bash, ...
    * Editing the command line
    * The two most useful commands: `ls` and `cd`
    * Wildcards `*`, `?`, `[..]`
    * Regular expressions
    * Navigating the file system                  
    * Printing on the command line: `echo`
    * Printing file content: `cat`, `head`, `tail`, `less`
    * Iterations with `for`, `do`, `done`
    * Conditions: `if`, `else if`, `else`, `fi`
    * Pipes `|` and redirections `>`, `<`
    * Scripts
    * Connecting to the cluster: ssh and scp
    * login nodes and compute nodes               
    * Compute Resources
    * Finding available compute nodes
    * Batch jobs                                  
    * Interactive jobs                            
    * The job scheduler SLURM
    * Useful SLURM commands                              
